import { Employee } from "./employee";
import { ILocation } from "./ilocation";

export class CompanyLocationLocalStorage implements ILocation {
  private localStorageKey: string;
  private persons: Employee[];

  constructor(localStorageKey: string) {
    this.localStorageKey = localStorageKey;
    this.persons = JSON.parse(localStorage.getItem(this.localStorageKey) || '[]',(key, value) => {
      if (key === "" && Array.isArray(value)) {
        return value.map(employeeData => new Employee(employeeData.name, employeeData.project));
      }
      return value;
    });
  }

  addPerson(person: Employee): void {
    this.persons.push(person);
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.persons));
  }

  getPerson(index: number): Employee | null {
    return this.persons[index] || null;
  }

  getCount(): number {
    return this.persons.length;
  }

  getPersons(): Employee []  {
    return this.persons;
  }
}

