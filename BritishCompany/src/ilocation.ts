import { Employee } from "./employee";

export interface ILocation {
    addPerson(person: Employee): void;
    getPerson(index: number): Employee | null;
    getCount(): number;
  }