import { CompanyLocationLocalStorage } from './companyLocalStorage';
import {Employee} from './employee';
import { ILocation } from './ilocation';

export class Company {
  private location: ILocation;
  private employees: Employee[] = [];

  constructor(location: ILocation) {
    this.location = location;
  }

  addEmployee(employee: Employee): void {
    this.employees.push(employee);
    this.location.addPerson(employee);
  }

  getProjectList(): string[] {
    if(this.location instanceof CompanyLocationLocalStorage){
      return this.location.getPersons().map((employee) => employee.getCurrentProject);
    }
    return this.employees.map((employee) => employee.getCurrentProject);
  }

  getNameList(): string[] {
    if(this.location instanceof CompanyLocationLocalStorage){
      return this.location.getPersons().map((employee) => employee.getName);
    }
    return this.employees.map((employee) => employee.getName);
  }

  getEmployeeCount(): number {
    return this.location.getCount();
  }
}

