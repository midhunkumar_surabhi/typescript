export class Employee {
    constructor(name, project) {
        this.name = name;
        this.project = project;
    }
    get getCurrentProject() {
        return this.project;
    }
    get getName() {
        return this.name;
    }
}
