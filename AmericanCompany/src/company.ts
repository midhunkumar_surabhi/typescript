import { IEmployee } from "./iemployee";

export class Company{

      private employees : IEmployee[] = [];

      public add(employee : IEmployee) : IEmployee{
        this.employees.push(employee);
        return employee;
      }

      get getProjectList() : String[]{
        return this.employees.map((emp) : String => emp.getProjectName);
      }

      get getNameList() : String[]{
        return this.employees.map((emp) : String => emp.getName);
      }


}