import { IEmployee } from "./iemployee";

export class Frontend implements IEmployee{

    constructor(public projectName : String, public name : String){}

    get getProjectName(): String {
        return this.projectName;
    }

    get getName(): String {
        return this.name;
    }
    
}