export interface IEmployee{
     projectName : String;
     name : String;
     get getProjectName() : String;
     get getName() : String;
}