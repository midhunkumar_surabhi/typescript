export class Backend {
    constructor(projectName, name) {
        this.projectName = projectName;
        this.name = name;
    }
    get getProjectName() {
        return this.projectName;
    }
    get getName() {
        return this.name;
    }
}
