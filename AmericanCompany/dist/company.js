export class Company {
    constructor() {
        this.employees = [];
    }
    add(employee) {
        this.employees.push(employee);
        return employee;
    }
    get getProjectList() {
        return this.employees.map((emp) => emp.getProjectName);
    }
    get getNameList() {
        return this.employees.map((emp) => emp.getName);
    }
}
