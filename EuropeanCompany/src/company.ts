
import {Employee} from "./employee";
export class Company{

    private employees : Employee[] =[];

    public  add(employee:Employee):Employee
    {
        this.employees.push(employee);
        return employee;
    }

   get getListOfEmployeeProjects():string[]{
        return this.employees.map(emp=>emp.getCurrentProject);
    }

    get getNameList():string[]{

        return this.employees.map(emp=>emp.getName);
    }


}

