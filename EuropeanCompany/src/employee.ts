export class Employee{
    constructor(private name:string, private projectName:string){
        
    }

    get getCurrentProject():string{
        return this.projectName;
    }

    get getName():string{
        return this.name;
    }
}

