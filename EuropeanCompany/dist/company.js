export class Company {
    constructor() {
        this.employees = [];
    }
    add(employee) {
        this.employees.push(employee);
        return employee;
    }
    get getListOfEmployeeProjects() {
        return this.employees.map(emp => emp.getCurrentProject);
    }
    get getNameList() {
        return this.employees.map(emp => emp.getName);
    }
}
