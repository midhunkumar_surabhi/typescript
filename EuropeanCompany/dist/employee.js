export class Employee {
    constructor(name, projectName) {
        this.name = name;
        this.projectName = projectName;
    }
    get getCurrentProject() {
        return this.projectName;
    }
    get getName() {
        return this.name;
    }
}
